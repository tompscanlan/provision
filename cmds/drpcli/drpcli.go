package main

import (
	"os"
	"strings"

	gojq "github.com/itchyny/gojq/cli"
	"gitlab.com/rackn/provision/v4/cli"
)

func main() {
	pgname := os.Args[0]
	pgname = strings.TrimSuffix(pgname, ".exe")
	if strings.HasSuffix(pgname, "jq") {
		os.Exit(gojq.Run())
	}
	err := cli.NewApp().Execute()
	if err != nil {
		os.Exit(1)
	}
}
