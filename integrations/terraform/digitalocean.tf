# Digital Ocean Quick Start
variable "drp_username" {
  type    = string
  default = "rocketskates"
}

variable "drp_password" {
  type    = string
  default = "r0cketsk8ts"
}

variable "drp_id" {
  type    = string
  default = "!default!"
}

variable "drp_version" {
  type    = string
  default = "tip"
}

variable "do_ssh_key" {
  type = string
}

# Configure the Digital Ocean provider
terraform {
  required_providers {
    linode = {
      source  = "digitalocean/digitalocean"
      version = ">= 2.21.0"
    }
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }
  required_version = ">= 1.0"
}

provider "digitalocean" {
  #token = """
}

locals {
  drp_id            = var.drp_id != "!default!" ? "--drp-id=${var.drp_id}" : ""
  cloud_init_script = <<-EOT
  #!/usr/bin/env bash
  value=$(curl -sfL curl http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)
  curl -fsSL get.rebar.digital/stable | sudo bash -s -- install --universal --version=${var.drp_version} --ipaddr=$value --drp-password=${var.drp_password} --remove-rocketskates --drp-user=${var.drp_username} ${local.drp_id}
  EOT
}

data "digitalocean_ssh_key" "terraform" {
  name = var.do_ssh_key
}

resource "digitalocean_droplet" "drp_machine" {
  name      = "drp-server"
  image     = "centos-stream-9-x64"
  region    = "nyc1"
  size      = "s-2vcpu-4gb"
  user_data = local.cloud_init_script
  tags      = ["digitalrebar"]
  ssh_keys  = [data.digitalocean_ssh_key.terraform.id]
}

output "drp_credentials" {
  value       = "export RS_KEY=${var.drp_username}:${var.drp_password}"
  description = "export of DRP credentials"
}

output "drp_manager" {
  value       = "export RS_ENDPOINT=https://${digitalocean_droplet.drp_machine.ipv4_address}:8092"
  description = "export of URL of DRP"
}
