package main

import (
	"encoding/json"
	"log"
	"slices"
)

type Hostvars map[string]interface{}

type InventoryGroup struct {
	Children     []string `json:"children,omitempty"`
	Hosts        []string `json:"hosts"`
	Vars         Params   `json:"vars"`
	Hostvars     Hostvars `json:"hostvars,omitempty"`
	RebarProfile string   `json:"rebar_profile,omitempty"`
	RebarUrl     string   `json:"rebar_url,omitempty"`
	RebarUser    string   `json:"rebar_user,omitempty"`
}

func NewInventoryGroup() InventoryGroup {
	return InventoryGroup{
		Children: make([]string, 0),
		Hosts:    make([]string, 0),
		Vars:     make(Params),
		Hostvars: make(Hostvars),
	}
}

type Inventory map[string]InventoryGroup

func NewInventory() *Inventory {

	inv := &Inventory{}
	all := NewInventoryGroup()
	meta := NewInventoryGroup()
	ungrouped := NewInventoryGroup()

	(*inv)["all"] = all
	(*inv)["_meta"] = meta
	(*inv)["ungrouped"] = ungrouped

	return inv
}

func (inv *Inventory) String() string {
	t, err := json.MarshalIndent(inv, "", "  ")
	if err != nil {
		log.Fatal(err)
	}
	return string(t)
}

// Update the inventory with the given machine's profiles.
//
//	When no profiles, add machine to ungrouped and ungrouped to all children.
//	When profiles, add machine to each profile and add profile to all children.
func (inventory *Inventory) updateInventoryUsingMachineProfiles(config *Config, machine *Machine) {

	all := (*inventory)["all"]

	// if machine has no profiles
	if len(machine.Profiles) == 0 {
		ungrouped := (*inventory)["ungrouped"]

		if !slices.Contains(ungrouped.Hosts, machine.Name) {
			ungrouped.Hosts = append(ungrouped.Hosts, machine.Name)
		}

		if !slices.Contains(all.Children, "ungrouped") {
			all.Children = append(all.Children, "ungrouped")
		}
		(*inventory)["ungrouped"] = ungrouped
		(*inventory)["all"] = all
		return
	}

	// for each profile
	for _, profile := range machine.Profiles {

		// Allow ignoring groups via config
		if slices.Contains(config.IgnoreGroups, profile) {
			continue
		}

		// Create the group if it doesn't exist
		if _, ok := (*inventory)[profile]; !ok {
			(*inventory)[profile] = NewInventoryGroup()
		}

		// Get the group and add the machine as a host into it
		group, ok := (*inventory)[profile]
		if ok {
			if !slices.Contains(group.Hosts, machine.Name) {
				group.Hosts = append(group.Hosts, machine.Name)
			}
		}

		// Put the modified group back into the inventory
		(*inventory)[profile] = group

		// Add the profile to the all group
		allChildren := (*inventory)["all"].Children
		if !slices.Contains(allChildren, profile) {
			allChildren = append(allChildren, profile)
		}
		all.Children = allChildren
		(*inventory)["all"] = all

	}

}

func (inventory *Inventory) getGroups() []string {
	groups := make([]string, 0)
	for k, _ := range *inventory {
		if k == "all" || k == "_meta" || k == "ungrouped" {
			continue
		}
		groups = append(groups, k)

	}
	return groups
}

func (inventory *Inventory) getHosts() []string {
	all := (*inventory)["all"]
	hosts := append([]string{}, all.Hosts...)
	return hosts
}

func (inventory *Inventory) updateInventoryUsingProfileParams(config *Config, groups Groups) {
	// for every group in the inventory, add the params to the group
	for _, profile := range groups {

		// group ought to exist, but if it doesn't, create it
		group, ok := (*inventory)[profile.Name]
		if !ok {
			if *config.Debug {
				log.Printf("WARN: group %s does not exist, creating it.\nIndicates a group that has no hosts", profile.Name)
			}
			group = NewInventoryGroup()
		}

		if slices.Contains(config.IgnoreGroups, profile.Name) {
			continue
		}
		if profile.Name == "all" || profile.Name == "_meta" || profile.Name == "ungrouped" {
			continue
		}

		group.Vars = profile.Params

		// for each profile, add it as a child group to the current profile
		for _, childgroup := range profile.Profiles {
			if slices.Contains(config.IgnoreGroups, childgroup) {
				continue
			}

			if !slices.Contains(group.Children, childgroup) {
				group.Children = append(group.Children, childgroup)
			}
		}

		(*inventory)[profile.Name] = group

	}
}

func (inv *Inventory) setDrpVars(config *Config) {

	meta, ok := (*inv)["_meta"]
	if !ok {
		meta = NewInventoryGroup()
	}
	meta.RebarUrl = config.Endpoint
	meta.RebarUser = config.User
	meta.RebarProfile = config.Profile

	(*inv)["_meta"] = meta
}
