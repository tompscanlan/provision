package main

// Based on info from
// https://docs.ansible.com/ansible/latest/dev_guide/developing_inventory.html#developing-inventory-scripts
//

import (
	"crypto/tls"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	config := NewConfig()

	inventory := NewInventory()

	handleFlags(config)

	setupLogging()
	getEnvironmentVars(config)
	client := makeHttpClient(*config)

	// if in host mode, get the single host vars and output
	if *config.Host != "" {
		if *config.Debug {
			log.Printf("INFO: in host mode for %s", *config.Host)
		}
		machine := getMachine(client, config, *config.Host)
		fmt.Println(&machine)
	} else {

		// if in list mode, get all machines
		if *config.List {

			if *config.Debug {
				log.Printf("INFO: in list mode")
			}
			machines := getMachines(client, config)
			machinesToInventory(config, machines, inventory)
			groups := getAllProfiles(client, config)
			inventory.updateInventoryUsingProfileParams(config, groups)
			inventory.setDrpVars(config)

			fmt.Print(inventory)
		}
	}

	// helper for allowing tests to run main() multiple times
	resetFlags()
}
func resetFlags() {
	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ExitOnError)
}

func setupLogging() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	log.SetOutput(os.Stderr)

}

func makeRequest(client *http.Client, config Config, url string) *http.Response {

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Add("Content-Type", "application/json")
	req.SetBasicAuth(config.User, config.Password)

	if config.Debug != nil && *config.Debug {
		log.Println("Making request to", url, "with user", config.User, "and pass", config.Password)
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	if resp.StatusCode != http.StatusOK {
		log.Fatal("Unexpected status code ", resp.StatusCode)
	}
	client.CloseIdleConnections()
	return resp
}

func makeHttpClient(config Config) *http.Client {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Transport: tr,
		Timeout:   10 * time.Second,
	}

	return client
}
