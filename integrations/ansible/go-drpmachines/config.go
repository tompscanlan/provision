package main

import (
	"flag"
	"fmt"
	"log"
)

type Config struct {
	Endpoint     string
	List         *bool   // if true, list all machines
	Host         *string // if not empty, list a single host vars
	Debug        *bool
	IgnoreSSL    bool
	Profile      string
	AnsibleUser  string
	ParentKey    string
	User         string
	Password     string
	IgnoreParams []string
	IgnoreGroups []string
}

func (config *Config) String() string {
	return fmt.Sprintf("Endpoint: %s, List: %t, Host: %s, Debug: %t, IgnoreSSL: %t, Profile: %s, AnsibleUser: %s, ParentKey: %s, User: %s, Password: %s", config.Endpoint, *config.List, *config.Host, *config.Debug, config.IgnoreSSL, config.Profile, config.AnsibleUser, config.ParentKey, config.User, config.Password)
}

func NewConfig() *Config {
	return &Config{
		IgnoreParams: []string{"gohai-inventory", "inventory/data", "change-stage/map"},
		IgnoreGroups: []string{"global", "rackn-license"},
	}
}

func handleFlags(config *Config) error {

	listFlag := flag.Lookup("list")

	if listFlag != nil && (*listFlag).Value.String() != "" {
		// handle flags has already been called
		// if config != nil && *config.Debug {
		log.Printf("WARN: called handleFlags() a second time, ignoring")
		// }
		return nil
	}

	config.List = flag.Bool("list", false, "Ansible dynamic inventory via DigitalRebar")
	config.Host = flag.String("host", "", "Ansible inventory of a particular host")
	config.Debug = flag.Bool("debug", false, "Enable debug logging")

	ignoreSSL := flag.Bool("k", false, "Ignore SSL certificate errors (short)")
	ignoreSSLLong := flag.Bool("ignore-ssl", false, "Ignore SSL certificate errors (long)")
	flag.Parse()
	temp := *ignoreSSL || *ignoreSSLLong
	config.IgnoreSSL = temp

	if config.Host != nil && *config.Host != "" && config.List != nil && *config.List {
		log.Fatal("cannot specify both --host and --list")
	}

	if *config.Debug {
		log.Printf("Config: %+v", config)
		log.Printf("Host2: %+v", *config.Host)
	}

	return nil
}
