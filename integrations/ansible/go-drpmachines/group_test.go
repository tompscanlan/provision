package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetProfiles(t *testing.T) {
	client, rec := makeRecordingClient(t, "fixtures/get-profiles")
	list := true
	debug := true
	profiles := getAllProfiles(client,
		&Config{
			List:      &list,
			Endpoint:  "https://192.168.1.3:8092",
			User:      "rocketskates",
			Password:  "r0cketsk8ts",
			IgnoreSSL: true,
			Debug:     &debug,
			Profile:   "all_machines",
		})
	assert.IsType(t, Groups{}, profiles, "Expected Groups type to match API response")
	rec.Stop()
}
