package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"slices"
)

type Machines []Machine

// type Machine map[string]interface{}

type Machine struct {
	Address           string      `json:"Address"`
	Arch              string      `json:"Arch"`
	Available         bool        `json:"Available"`
	BootEnv           string      `json:"BootEnv"`
	Bundle            string      `json:"Bundle"`
	Context           string      `json:"Context"`
	CurrentJob        string      `json:"CurrentJob"`
	CurrentTask       int64       `json:"CurrentTask"`
	Description       string      `json:"Description"`
	Endpoint          string      `json:"Endpoint"`
	Errors            []string    `json:"Errors"`
	Fingerprint       Fingerprint `json:"Fingerprint"`
	HardwareAddrs     []string    `json:"HardwareAddrs"`
	JobExitState      string      `json:"JobExitState"`
	JobResultErrors   []string    `json:"JobResultErrors"`
	JobState          string      `json:"JobState"`
	Locked            bool        `json:"Locked"`
	Meta              Meta        `json:"Meta"`
	Name              string      `json:"Name"`
	OS                string      `json:"OS"`
	Params            Params      `json:"Params"`
	Partial           bool        `json:"Partial"`
	PendingWorkOrders int64       `json:"PendingWorkOrders"`
	Pool              string      `json:"Pool"`
	PoolAllocated     bool        `json:"PoolAllocated"`
	PoolStatus        string      `json:"PoolStatus"`
	Profiles          []string    `json:"Profiles"`
	ReadOnly          bool        `json:"ReadOnly"`
	RetryTaskAttempt  int64       `json:"RetryTaskAttempt"`
	Runnable          bool        `json:"Runnable"`
	RunningWorkOrders int64       `json:"RunningWorkOrders"`
	Secret            string      `json:"Secret"`
	Stage             string      `json:"Stage"`
	TaskErrorStacks   []string    `json:"TaskErrorStacks"`
	Tasks             []string    `json:"Tasks"`
	UUID              string      `json:"Uuid"`
	Validated         bool        `json:"Validated"`
	WorkOrderMode     bool        `json:"WorkOrderMode"`
	Workflow          string      `json:"Workflow"`
	WorkflowComplete  bool        `json:"WorkflowComplete"`
}
type Params map[string]interface{}

type Fingerprint struct {
	SSNHash         string        `json:"SSNHash"`
	CSNHash         string        `json:"CSNHash"`
	CloudInstanceID string        `json:"CloudInstanceID"`
	SystemUUID      string        `json:"SystemUUID"`
	MemoryIDS       []interface{} `json:"MemoryIds"`
}

type Meta struct {
	Pipeline     string `json:"pipeline"`
	PipelineUUID string `json:"pipeline-uuid"`
	Color        string `json:"color"`
	FeatureFlags string `json:"feature-flags"`
	Icon         string `json:"icon"`
	MachineRole  string `json:"machine-role"`
}

func NewMachine() *Machine {
	return &Machine{
		Params: make(Params),
	}
}

func (machine *Machine) String() string {

	t, err := json.MarshalIndent(machine, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	// if machine is empty, return empty json object
	if machine.Name == "" && machine.Address == "" {
		return "{}"
	}
	return string(t)

}

func getMachine(client *http.Client, config *Config, machineName string) Machine {

	url := fmt.Sprintf("%s/api/v3/machines?Name=%s", config.Endpoint, machineName)
	response := makeRequest(client, *config, url)

	if response.StatusCode != http.StatusOK {
		log.Fatal("An Unexpected status code ", response.StatusCode)
	}
	body, error := io.ReadAll(response.Body)
	defer response.Body.Close()

	if error != nil {
		log.Fatal(error)
	}

	// log.Printf("INFO: body, %s", body)
	var machines Machines
	err := json.Unmarshal([]byte(body), &machines)
	if err != nil {
		log.Fatalf("expected to see a list of machines: %v", err)
	}
	if len(machines) == 0 {
		return *NewMachine()
	}
	if len(machines) > 1 {
		log.Printf("WARN: more than one machine found, using first")
	}

	return machines[0]
}

func getMachines(client *http.Client, config *Config) *Machines {

	url := config.Endpoint + "/api/v3/machines"

	response := makeRequest(client, *config, url)

	if response.StatusCode != http.StatusOK {
		log.Fatal("An Unexpected status code ", response.StatusCode)
	}
	body, error := io.ReadAll(response.Body)
	defer response.Body.Close()

	if error != nil {
		log.Fatal(error)
	}

	var machines Machines
	err := json.Unmarshal([]byte(body), &machines)
	if err != nil {
		log.Fatalf("expected to see a list of machines: %v", err)
	}

	return &machines
}

func checkMandatoryMachineFields(config *Config, machine *Machine) error {
	if machine.Name == "" {
		return fmt.Errorf("machine name is empty")
	}
	if machine.Address == "" {
		return fmt.Errorf("machine address is empty")
	}
	return nil
}

func captureMachineVars(config *Config, machine *Machine) (Hostvars, error) {
	myvars := make(map[string]interface{})

	myvars["ansible_host"] = machine.Address

	ansibleUser, ok := machine.Params["ansible_user"].(string)
	if ok && ansibleUser != "" {
		myvars["ansible_user"] = ansibleUser
	} else if config.AnsibleUser != "" {
		myvars["ansible_user"] = config.AnsibleUser
	}

	if machine.UUID != "" {
		myvars["rebar_uuid"] = machine.UUID
	}

	for k, v := range machine.Params {
		if slices.Contains(config.IgnoreParams, k) {
			continue
		}
		myvars[k] = v
	}

	return myvars, nil
}

func machinesToInventory(config *Config, machines *Machines, inventory *Inventory) {

	for _, machine := range *machines {
		machineToInventory(config, &machine, inventory)
	}
}

// Update the inventory with the given machine's hostvars
func machineToInventory(config *Config, machine *Machine, inventory *Inventory) error {
	err := checkMandatoryMachineFields(config, machine)
	if err != nil {
		return err
	}

	myvars, err := captureMachineVars(config, machine)
	if err != nil {
		return err
	}

	meta := (*inventory)["_meta"]

	meta.Hostvars[machine.Name] = myvars
	inventory.updateInventoryUsingMachineProfiles(config, machine)

	(*inventory)["_meta"] = meta

	return nil
}
