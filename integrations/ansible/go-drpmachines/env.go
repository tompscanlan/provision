package main

import (
	"log"
	"os"
	"strings"
)

func getEnvironmentVars(config *Config) {
	config.Endpoint = os.Getenv("RS_ENDPOINT")
	if config.Endpoint == "" {
		config.Endpoint = "https://127.0.0.1:8092"
	}

	userPass := os.Getenv("RS_KEY")
	if userPass == "" {
		userPass = "rocketskates:r0cketsk8ts"
	}
	config.User, config.Password = splitUserPassword(userPass)

	config.Profile = os.Getenv("RS_ANSIBLE")
	if config.Profile == "" {
		config.Profile = "all_machines"
	}

	config.AnsibleUser = os.Getenv("RS_ANSIBLE_USER")
	if config.AnsibleUser == "" {
		config.AnsibleUser = "root"
	}

	config.ParentKey = os.Getenv("RS_ANSIBLE_PARENT")
	if config.ParentKey == "" {
		config.ParentKey = "ansible/children"
	}

	if *config.Debug {
		log.Printf("Config: %+v", config)
	}
}

func splitUserPassword(ups string) (string, string) {
	split := strings.SplitN(ups, ":", 2)
	return split[0], split[1]
}
