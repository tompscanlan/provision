package main

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"os/exec"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/dnaeon/go-vcr.v3/cassette"
	"gopkg.in/dnaeon/go-vcr.v3/recorder"
)

func TestGroupsToInventory(t *testing.T) {
	t.SkipNow()
	// i := NewInventory()

	// b, err := json.Marshal(i)
	// if err != nil {

	// 	log.Fatal(err)
	// }
	// log.Printf("Inventory: %+v", string(b))

	// given_groups := Groups{}
	// expected_inventory := Inventory{}
	// found_inventory := &Inventory{}

	// given_config := Config{}

	// groupsToInventory(given_config, given_groups, found_inventory)
	// assert.JSONEq(t, expected_inventory, *found_inventory, "Expected %s, got %s", expected_inventory, *found_inventory)

}

func TestBinaryExitCode(t *testing.T) {
	t.SkipNow()
	cmd := exec.Command("./your-binary-name", "--debug", "--host", "somehost", "--list")

	expectedExitCode := 1
	err := cmd.Run()

	exitError, ok := err.(*exec.ExitError)
	if !ok {
		t.Fatalf("Command did not exit with an error, got: %v", err)
	}

	if status := exitError.ExitCode(); status != expectedExitCode {
		t.Errorf("Expected exit code %d, got %d", expectedExitCode, status)
	}
}

// func TestHostListExclusivity(t *testing.T) {
// 	t.Skip("Skipping test... need to test for exit")
// 	givenConfig := &Config{}
// 	os.Args = []string{"--debug", "--host", "somehost", "--list"}

// 	defer func() {
// 		if r := recover(); r == nil {
// 			t.Errorf("The code did not panic")
// 		}
// 	}()

// 	handleFlags(givenConfig)
// }

// this client records and replays http requests for tests
func makeRecordingClient(t *testing.T, fixturepath string) (*http.Client, *recorder.Recorder) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	r, err := recorder.NewWithOptions(
		&recorder.Options{
			CassetteName:       fixturepath,
			Mode:               recorder.ModeReplayWithNewEpisodes,
			SkipRequestLatency: true,
			RealTransport:      tr,
		})
	if err != nil {
		t.Fatal(err)
	}
	// defer r.Stop() // Make sure recorder is stopped once done with it

	client := r.GetDefaultClient()

	hook := func(i *cassette.Interaction) error {
		delete(i.Request.Headers, "Authorization")

		return nil
	}
	r.AddHook(hook, recorder.AfterCaptureHook)

	return client, r
}

func TestMainList(t *testing.T) {
	given := `[]`

	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth := r.Header.Get("Authorization")
		assert.NotEmpty(t, auth, "Expected Authorization header to be set")

		assert.True(t,
			strings.Contains(r.URL.String(), "machines") ||
				strings.Contains(r.URL.String(), "profiles"),
			"Expected URL to contain machines or profiles")

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(given))
	}))
	defer server.Close()

	expected := fmt.Sprintf(`{
		"_meta": {
		  "hosts": [],
		  "vars": {},
		  "rebar_profile": "all_machines",
		  "rebar_url": "%s",
		  "rebar_user": "rocketskates"
		},
		"all": {
		  "hosts": [],
		  "vars": {}
		},
		"ungrouped": {
		  "hosts": [],
		  "vars": {}
		}
	  }`, server.URL)

	// capture stdout
	old := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	defer func() { os.Stdout = old }()
	outC := make(chan string)
	go func() {
		var buf bytes.Buffer
		io.Copy(&buf, r)
		outC <- buf.String()
	}()

	// point requests at mock server
	os.Setenv("RS_ENDPOINT", server.URL)
	os.Args = []string{"program", "--list"}
	main()

	w.Close()
	out := <-outC
	// restore stdout
	os.Stdout = old

	assert.JSONEqf(t, expected, out, "Expected %s, got %s", expected, out)

}

func TestMainNonExistentHost(t *testing.T) {
	given := `[]`
	expected := `{}`

	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth := r.Header.Get("Authorization")
		assert.NotEmpty(t, auth, "Expected Authorization header to be set")

		assert.True(t,
			strings.Contains(r.URL.String(), "machines") ||
				strings.Contains(r.URL.String(), "profiles"),
			"Expected URL to contain machines or profiles")

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(given))
	}))
	defer server.Close()

	// capture stdout
	old := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	defer func() { os.Stdout = old }()
	outC := make(chan string)
	go func() {
		var buf bytes.Buffer
		io.Copy(&buf, r)
		outC <- buf.String()
	}()

	// point requests at mock server
	os.Setenv("RS_ENDPOINT", server.URL)
	os.Args = []string{"program", "--host", "nonexistent"}
	main()

	w.Close()
	out := <-outC
	// restore stdout
	os.Stdout = old

	assert.JSONEqf(t, expected, out, "Expected %s, got %s", expected, out)

}
func TestOldVsNew(t *testing.T) {
	t.Skip("Skipping because, we're very close, but not identical")
	// read in file o.json and original-no-args.txt and compare them as json deep

	original, err := os.Open("fixtures/original-no-args.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer original.Close()

	newer, err := os.Open("fixtures/o.json")
	if err != nil {
		log.Fatal(err)
	}
	defer newer.Close()

	originalBytes, err := io.ReadAll(original)
	if err != nil {
		log.Fatal(err)
	}

	newerBytes, err := io.ReadAll(newer)
	if err != nil {
		log.Fatal(err)
	}

	originalString := string(originalBytes)
	newerString := string(newerBytes)

	assert.JSONEq(t, originalString, newerString)

	// , "Expected %s, got %s", original, newerString)
}
