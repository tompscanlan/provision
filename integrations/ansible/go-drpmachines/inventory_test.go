package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMachineToInventoryTwo(t *testing.T) {
	given := &Machine{
		Name:    "workerA.attlocal.net",
		Bundle:  "Not Copied",
		Address: "10.10.1.2",
		Params: map[string]interface{}{
			"ansible_user": "root",
			"ipmi/enabled": false,
		},
		Profiles: []string{"worker", "bootstrap-tools"},
	}
	expectedInventory := `{
		"_meta": {
		  "hosts": [],
		  "vars": {},
		  "hostvars": {
			"workerA.attlocal.net": {
			  "ansible_host": "10.10.1.2",
			  "ansible_user": "root",
			  "ipmi/enabled": false
			}
		  }
		},
		"all": {
		  "children": [
			"worker",
			"bootstrap-tools"
		  ],
		  "hosts": [],
		  "vars": {}
		},
		"bootstrap-tools": {
		  "hosts": [
			"workerA.attlocal.net"
		  ],
		  "vars": {}
		},
		"ungrouped": {
		  "hosts": [],
		  "vars": {}
		},
		"worker": {
		  "hosts": [
			"workerA.attlocal.net"
		  ],
		  "vars": {}
		}
	  }`

	inventory := NewInventory()
	debug := false
	err := machineToInventory(&Config{Debug: &debug}, given, inventory)
	if err != nil {
		t.Errorf("Expected no error, got %s", err)
	}

	assert.JSONEq(t, expectedInventory, inventory.String(), "Expected %s, got %s", expectedInventory, inventory)

}

func TestMachineToInventoryUngrouped(t *testing.T) {
	given := &Machine{
		Name:    "workerA.attlocal.net",
		Address: "192.168.1.6",
		Params: map[string]interface{}{
			"ansible_user": "root",
			"ipmi/enabled": false,
		},
		Profiles: []string{},
	}
	expectedInventory := `{
		"_meta": {
		  "hosts": [],
		  "vars": {},
		  "hostvars": {
			"workerA.attlocal.net": {
			  "ansible_host": "192.168.1.6",
			  "ansible_user": "root",
			  "ipmi/enabled": false
			}
		  }
		},
		"all": {
		  "children": [
			"ungrouped"
		  ],
		  "hosts": [],
		  "vars": {}
		},
		"ungrouped": {
		  "hosts": [
			"workerA.attlocal.net"
		  ],
		  "vars": {}
		}
	  }`

	inventory := NewInventory()

	err := machineToInventory(&Config{}, given, inventory)
	if err != nil {
		t.Errorf("Expected no error, got %s", err)
	}
	assert.JSONEq(t, expectedInventory, inventory.String(), "Expected %s, got %s", expectedInventory, inventory)

}

func TestUpdateInvUsingMachineProfiles(t *testing.T) {
	given := &Machine{
		Name:    "workerA.attlocal.net",
		Address: "10.10.1.2",
		Params: map[string]interface{}{
			"ansible_user": "root",
			"ipmi/enabled": false,
		},
		Profiles: []string{"worker", "bootstrap-tools"},
	}
	expectedInventory := `{
		"_meta": {
		  "hosts": [],
		  "vars": {}
		},
		"all": {
		  "children": [
			"worker",
			"bootstrap-tools"
		  ],
		  "hosts": [],
		  "vars": {}
		},
		"bootstrap-tools": {
		  "hosts": [
			"workerA.attlocal.net"
		  ],
		  "vars": {}
		},
		"ungrouped": {
		  "hosts": [],
		  "vars": {}
		},
		"worker": {
		  "hosts": [
			"workerA.attlocal.net"
		  ],
		  "vars": {}
		}
	  }`
	inventory := NewInventory()
	debug := false
	inventory.updateInventoryUsingMachineProfiles(&Config{Debug: &debug}, given)

	assert.JSONEq(t, expectedInventory, inventory.String(), "Expected %s, got %s", expectedInventory, inventory)
}

func TestGetGroupsWhenUngrouped(t *testing.T) {
	given := &Machine{
		Name:    "workerA.attlocal.net",
		Address: "10.10.1.2",
		Params: map[string]interface{}{
			"ansible_user": "root",
			"ipmi/enabled": false,
		},
		Profiles: []string{},
	}

	expectedGroups := []string{}

	inv := NewInventory()
	machineToInventory(&Config{}, given, inv)
	groups := inv.getGroups()

	assert.Equal(t, expectedGroups, groups, "Expected %s, got %s", expectedGroups, groups)

	assert.NotNil(t, (*inv)["ungrouped"], "Expected ungrouped to be present")
	assert.Equal(t, []string{"workerA.attlocal.net"}, (*inv)["ungrouped"].Hosts, "Expected ungrouped to contain the machine")
}

func TestGetGroups(t *testing.T) {
	given := &Machine{
		Name:    "workerA.attlocal.net",
		Address: "10.10.1.2",
		Params: map[string]interface{}{
			"ansible_user": "root",
			"ipmi/enabled": false,
		},
		Profiles: []string{"worker", "bootstrap-tools"},
	}

	expectedGroups := []string{"worker", "bootstrap-tools"}

	inv := NewInventory()
	machineToInventory(&Config{}, given, inv)
	groups := inv.getGroups()

	assert.ElementsMatch(t, expectedGroups, groups, "Expected %s, got %s", expectedGroups, groups)
}
func TestGetLotsOfGroups(t *testing.T) {
	var randomGroups []string
	for i := 0; i < 100; i++ {
		randomGroups = append(randomGroups, fmt.Sprintf("group%d", i))
	}

	given := &Machine{
		Name:    "workerA.attlocal.net",
		Address: "10.10.1.2",
		Params: map[string]interface{}{
			"ansible_user": "root",
			"ipmi/enabled": false,
		},
		Profiles: randomGroups,
	}

	expectedGroups := randomGroups

	inv := NewInventory()
	machineToInventory(&Config{}, given, inv)
	groups := inv.getGroups()

	assert.ElementsMatch(t, expectedGroups, groups, "Expected %s, got %s", expectedGroups, groups)
}

func TestMachineToInventory(t *testing.T) {
	given := &Machine{
		Name:    "workerA.attlocal.net",
		Address: "192.168.1.6",
		Bundle:  "Not Copied",
		Params: map[string]interface{}{
			"ansible_user": "root",
			"ipmi/enabled": false,
		},
		Profiles: []string{"worker", "bootstrap-tools"},
	}
	expectedInventory := `{
		"_meta": {
		  "hosts": [],
		  "vars": {},
		  "hostvars": {
			"workerA.attlocal.net": {
			  "ansible_host": "192.168.1.6",
			  "ansible_user": "root",
			  "ipmi/enabled": false
			}
		  }
		},
		"all": {
		  "children": [
			"worker",
			"bootstrap-tools"
		  ],
		  "hosts": [],
		  "vars": {}
		},
		"bootstrap-tools": {
		  "hosts": [
			"workerA.attlocal.net"
		  ],
		  "vars": {}
		},
		"ungrouped": {
		  "hosts": [],
		  "vars": {}
		},
		"worker": {
		  "hosts": [
			"workerA.attlocal.net"
		  ],
		  "vars": {}
		}
	  }`

	inventory := NewInventory()

	machineToInventory(&Config{}, given, inventory)

	// fmt.Printf("Inventory: %+v", inventory)
	assert.JSONEq(t, expectedInventory, inventory.String(), "Expected %s, got %s", expectedInventory, inventory)

}

func TestMachinesToInventory(t *testing.T) {
	givenMachines := &Machines{
		Machine{
			Name:    "workerA.attlocal.net",
			Address: "10.1.1.1",
			Params: map[string]interface{}{
				"ansible_host": "workerA.attlocal.net",
				"ansible_user": "mean",
			},
		},
		Machine{
			Name:    "workerB.attlocal.net",
			Address: "10.1.1.2",
			Params: map[string]interface{}{
				"ansible_host": "workerB.attlocal.net",
				"ansible_user": "gean",
			},
		},
	}
	expectedInventory := `{
		"_meta": {
		  "hosts": [],
		  "vars": {},
		  "hostvars": {
			"workerA.attlocal.net": {
			  "ansible_host": "workerA.attlocal.net",
			  "ansible_user": "mean"
			},
			"workerB.attlocal.net": {
			  "ansible_host": "workerB.attlocal.net",
			  "ansible_user": "gean"
			}
		  }
		},
		"all": {
		  "children": [
			"ungrouped"
		  ],
		  "hosts": [],
		  "vars": {}
		},
		"ungrouped": {
		  "hosts": [
			"workerA.attlocal.net",
			"workerB.attlocal.net"
		  ],
		  "vars": {}
		}
	  }`

	foundInventory := NewInventory()

	config := Config{}
	machinesToInventory(&config, givenMachines, foundInventory)

	assert.JSONEq(t, expectedInventory, foundInventory.String(), "Expected %s, got %s", expectedInventory, foundInventory)

}

func TestGetHostsEmpty(t *testing.T) {
	given := NewInventory()
	expectedHosts := []string{}

	hosts := given.getHosts()

	assert.Equal(t, expectedHosts, hosts, "Expected %s, got %s", expectedHosts, hosts)
}

func TestGetHosts(t *testing.T) {
	hosts := []string{"workerA.attlocal.net", "workerB.attlocal.net"}
	given := NewInventory()
	(*given)["all"] = InventoryGroup{
		Hosts: hosts,
	}
	expectedHosts := hosts

	foundHosts := given.getHosts()

	assert.ElementsMatch(t, expectedHosts, foundHosts, "Expected %s, got %s", expectedHosts, foundHosts)
}

func TestUpdateInventoryUsingProfileParams(t *testing.T) {

	debug := true
	config := &Config{
		Debug: &debug,
	}

	inv := NewInventory()
	groups := Groups{
		Group{
			Name: "worker",
			Params: map[string]interface{}{
				"ansible_user": "root",
				"ipmi/enabled": false,
			},
		},
	}
	expectedInventory := `{
		"_meta": {
		  "hosts": [],
		  "vars": {}
		},
		"all": {
		  "hosts": [],
		  "vars": {}
		},
		"ungrouped": {
		  "hosts": [],
		  "vars": {}
		},
		"worker": {
		  "hosts": [],
		  "vars": {
			"ansible_user": "root",
			"ipmi/enabled": false
		  }
		}
	  }`

	inv.updateInventoryUsingProfileParams(config, groups)

	assert.JSONEq(t, expectedInventory, inv.String(), "Expected %s, got %s", expectedInventory, inv)

}

func TestUpdateInventoryUsingProfileParamsWithIgnoreGroups(t *testing.T) {

	debug := true
	config := &Config{
		Debug:        &debug,
		IgnoreGroups: []string{"worker"},
	}

	inv := NewInventory()
	groups := Groups{
		Group{
			Name: "worker",
			Params: map[string]interface{}{
				"ansible_user": "root",
				"ipmi/enabled": false,
			},
		},
	}
	expectedInventory := `{
		"_meta": {
		  "hosts": [],
		  "vars": {}
		},
		"all": {
		  "hosts": [],
		  "vars": {}
		},
		"ungrouped": {
		  "hosts": [],
		  "vars": {}
		}
	  }`

	inv.updateInventoryUsingProfileParams(config, groups)

	assert.JSONEq(t, expectedInventory, inv.String(), "Expected %s, got %s", expectedInventory, inv)

}
