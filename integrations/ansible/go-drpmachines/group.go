package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

type Group struct {
	Available     bool     `json:"Available"`
	Bundle        string   `json:"Bundle"`
	Description   string   `json:"Description"`
	Documentation string   `json:"Documentation"`
	Endpoint      string   `json:"Endpoint"`
	Errors        []any    `json:"Errors"`
	Meta          Params   `json:"Meta"`
	Name          string   `json:"Name"`
	Params        Params   `json:"Params"`
	Partial       bool     `json:"Partial"`
	Profiles      []string `json:"Profiles"`
	ReadOnly      bool     `json:"ReadOnly"`
	Validated     bool     `json:"Validated"`
}
type Groups []Group

func (g *Group) String() string {
	t, err := json.MarshalIndent(g, "", "  ")
	if err != nil {
		log.Fatal(err)
	}
	return string(t)
}
func NewGroup() *Group {
	return &Group{}
}

func getAllProfiles(client *http.Client, config *Config) Groups {
	profiles := makeRequest(client, *config, config.Endpoint+"/api/v3/profiles")

	body, error := io.ReadAll(profiles.Body)
	defer profiles.Body.Close()

	if error != nil {
		log.Fatal(error)
	}

	var groups Groups

	err := json.Unmarshal([]byte(body), &groups)
	if err != nil {
		log.Fatalf("expected to see a list of groups: %v", err)
	}

	return groups
}
