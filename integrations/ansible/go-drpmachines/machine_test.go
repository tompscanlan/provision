package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewMachine(t *testing.T) {
	m := NewMachine()
	assert.NotNil(t, m, "Expected NewMachine() to return a non-nil value")
}

func TestGetMachine(t *testing.T) {
	client, rec := makeRecordingClient(t, "fixtures/get-machine")
	defer rec.Stop()
	host := "workerB.attlocal.net"
	debug := true
	machine := getMachine(client,
		&Config{
			Host:      &host,
			Endpoint:  "https://192.168.1.3:8092",
			User:      "rocketskates",
			Password:  "r0cketsk8ts",
			IgnoreSSL: true,
			Debug:     &debug,
			Profile:   "all_machines",
		},
		host)

	assert.IsType(t, Machine{}, machine, "Expected Machine type to match API response")

	assert.Equal(t, host, machine.Name, "Expected host to match API response")
	assert.IsType(t, Params{}, machine.Params, "Expected Params to be a map")
}

func TestGetMachines(t *testing.T) {
	client, rec := makeRecordingClient(t, "fixtures/get-machines")
	list := true
	debug := false
	machines := getMachines(client, &Config{
		List:      &list,
		Endpoint:  "https://192.168.1.3:8092",
		User:      "rocketskates",
		Password:  "r0cketsk8ts",
		IgnoreSSL: true,
		Debug:     &debug,
		Profile:   "all_machines",
	})
	rec.Stop()

	assert.IsType(t, Machines{}, *machines, "Expected Machines type to match API response")

	assert.Equal(t, 10, len(*machines), "Expected set number of machines to be returned")
	// log.Printf("Machines: %+v", *machines)
}

func TestCaptureMachineVars(t *testing.T) {
	given := &Machine{
		Name:    "workerA.attlocal.net",
		Address: "192.168.1.6",
		Bundle:  "Not Copied",
		Params: map[string]interface{}{
			"ansible_user": "root",
			"ipmi/enabled": false,
		},
		Profiles: []string{"worker", "bootstrap-tools"},
	}
	expected := Hostvars{
		"ansible_host": "192.168.1.6",
		"ansible_user": "root",
		"ipmi/enabled": false,
	}

	vars, err := captureMachineVars(&Config{}, given)
	if err != nil {
		t.Errorf("Expected no error, got %s", err)
	}

	assert.Equal(t, expected, vars, "Expected vars to match")
}

func TestCaptureMachineVarsAnibleUserFromEnv(t *testing.T) {
	given := &Machine{
		Name:    "workerA.attlocal.net",
		Address: "192.168.1.6",
		Bundle:  "Not Copied",
		Params: map[string]interface{}{
			"ipmi/enabled": false,
		},
		Profiles: []string{"worker", "bootstrap-tools"},
	}
	expected := Hostvars{
		"ansible_host": "192.168.1.6",
		"ansible_user": "fromenv",
		"ipmi/enabled": false,
	}

	vars, err := captureMachineVars(&Config{AnsibleUser: "fromenv"}, given)

	if err != nil {
		t.Errorf("Expected no error, got %s", err)
	}

	assert.Equal(t, expected, vars, "Expected vars to match")
}
