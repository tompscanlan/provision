package models

import (
	"net"

	"github.com/pborman/uuid"
)

// Reservation tracks persistent DHCP IP address reservations.
//
// swagger:model
type Reservation struct {
	Validation
	Access
	Meta
	Owned
	Bundled
	// Addr is the IP address permanently assigned to the strategy/token combination.
	//
	// required: true
	// swagger:strfmt ipv4
	Addr net.IP `index:",key"`
	// A description of this Reservation.  This should tell what it is for,
	// any special considerations that should be taken into account when
	// using it, etc.
	Description string
	// Documentation of this reservation.  This should tell what
	// the reservation is for, any special considerations that
	// should be taken into account when using it, etc. in rich structured text (rst).
	Documentation string
	// Token is the unique identifier that the strategy for this Reservation should use.
	//
	// required: true
	Token string
	// Subnet is the name of the Subnet that this Reservation is associated with.
	// This property is read-only.
	//
	Subnet string
	// NextServer is the address the server should contact next. You
	// should only set this if you want to talk to a DHCP or TFTP server
	// other than the one provided by dr-provision.
	//
	// required: false
	// swagger:strfmt ipv4
	NextServer net.IP
	// Options is the list of DHCP options that apply to this Reservation
	Options []DhcpOption
	// Strategy is the leasing strategy that will be used determine what to use from
	// the DHCP packet to handle lease management.
	//
	// required: true
	Strategy string
	// Scoped indicates that this reservation is tied to a particular Subnet,
	// as determined by the reservation's Addr.
	//
	// required: true
	Scoped bool
	// Duration is the time in seconds for which a lease can be valid.
	// ExpireTime is calculated from Duration.
	Duration int32
	// Allocated indicates this is a reapable reservation
	Allocated bool `json:"Allocated,omitempty"`
	// Machine is the associated machine
	Machine uuid.UUID `json:"Machine,omitempty"`
	// Parameter is the parameter that this address should be stored in for the machine if specified
	Parameter string `json:"Parameter,omitempty"`
	// Params are any additional parameters that may be needed to expand templates
	// for BootEnv, as documented by that boot environment's
	// RequiredParams and OptionalParams.
	Params map[string]interface{}
	// Profiles that should be considered for parameters
	Profiles []string
	// PrefixParameter a string that should be the beginning of a set of option-based parameters
	PrefixParameter string `json:",omitempty"`
}

func (r *Reservation) GetMeta() Meta {
	return r.Meta
}

func (r *Reservation) SetMeta(d Meta) {
	r.Meta = d
}

// GetDocumentation returns the object's documentation
func (r *Reservation) GetDocumentation() string {
	return r.Documentation
}

// GetDescription returns the object's description
func (r *Reservation) GetDescription() string {
	return r.Description
}

func (r *Reservation) Prefix() string {
	return "reservations"
}

func (r *Reservation) Key() string {
	return Hexaddr(r.Addr)
}

func (r *Reservation) KeyName() string {
	return "Addr"
}

func (r *Reservation) Fill() {
	r.Validation.fill(r)
	if r.Meta == nil {
		r.Meta = Meta{}
	}
	if r.Options == nil {
		r.Options = []DhcpOption{}
	}
	if r.Profiles == nil {
		r.Profiles = []string{}
	}
	if r.Params == nil {
		r.Params = map[string]interface{}{}
	}
}

func (r *Reservation) Validate() {
	for k := range r.Params {
		r.AddError(ValidParamName("Invalid Param Name", k))
	}
	for _, v := range r.Profiles {
		r.AddError(ValidNumberName("Invalid Profile Name", v))
	}
}

func (r *Reservation) AuthKey() string {
	return r.Key()
}

func (r *Reservation) SliceOf() interface{} {
	s := []*Reservation{}
	return &s
}

func (r *Reservation) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Reservation)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

func (r *Reservation) CanHaveActions() bool {
	return true
}

// Match Paramer interface
// GetParams returns the current parameters for this profile
// matches Paramer interface
func (r *Reservation) GetParams() map[string]interface{} {
	return copyMap(r.Params)
}

// SetParams sets the current parameters for this profile
// matches Paramer interface
func (r *Reservation) SetParams(pl map[string]interface{}) {
	r.Params = copyMap(pl)
}

// match Profiler interface
// GetProfiles returns the profiles on this profile
func (r *Reservation) GetProfiles() []string {
	return r.Profiles
}

// SetProfiles sets the profiles on this profile
func (r *Reservation) SetProfiles(np []string) {
	r.Profiles = np
}
