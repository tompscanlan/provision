.. Copyright (c) 2017 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; Metadata

.. _rs_data_metadata_params:

Object.Meta
===========

Metadata values are used throughtout Digital Rebar for both UX and operational controls.  Please

This page provides a comprehensive list of Meta. values by object type and their use.

The following items are common to all objects:

* ``icon: [icon name]`` - sets the icon
* ``color: [color name]`` - set the color

.. _rs_param_metadata:

Param.Meta Options for the UX
-----------------------------

There are several meta fields that can be used to adjust on screen display for Params.

The following items are commonly used on all Object types to provide UX rendering guides:

* ``password: [anyvalue]`` - renders as password (NOT encrypted, just obfuscated)
* ``clipboard: [anyvalue]`` - provides a copy button so user can copy the param contents to clipboard.
* ``readonly: [anyvalue]`` - does not allow UX editing
* ``render: link`` - adds an https://[value] link field that opens in a new tab.
* ``render: multiline`` - presents a textarea for string inputs
* ``render: raw`` - presents textarea to user instead of regular edit, does not reformat
* ``downloadable: [file name]`` - provides download link so user can download the param contents as a file.  Name of the file is the value of the Meta.downloadable.
* ``route: [object name]`` - when included, the UX will provide a link to the underlying object as per the Param value

.. _rs_machine_metadata:

Machine.Meta Options
--------------------

Used by Digital Rebar server to manage special behavior.  See :ref:`rs_data_metadata_params_old` for more detail.

* ``BaseContext: [context]`` lets DRP know what the base context is for the machine when going back to a known context
* ``machine-role: [role]`` is set by DRP and should not be edited.  It lets DRP know what the type of machine this object is.  Values can be: ``self``, ``machine``, ``cluster``, or ``resource-broker``.
* ``pipeline:``: the name of the executed pipeline and set by Digital Rebar during Pipeline execution to allow operators to trace jobs across a pipeline run.
* ``pipeline-uuid``": is a unique ID for each executed pipeline and set by Digital Rebar during Pipeline execution to allow operators to trace jobs across a pipeline run.

.. _rs_cluster_metadata:

Cluster.Meta controls
---------------------

Used by Digital Rebar server to manage special behavior.  See :ref:`rs_data_metadata_params_old` for more detail.

* ``no-cluster: "true"`` tells the cluster and resource broker create that this parameter or profile should *NOT* be added to the special profile during object create.
* ``BaseContext: [context]`` lets DRP know what the base context is for the machine when going back to a known context
* ``machine-role: [role]`` is set by DRP and should not be edited.  It lets DRP know what the type of machine this object is.  Values can be: ``self``, ``machine``, ``cluster``, or ``resource-broker``.
* ``pipeline:``: the name of the executed pipeline and set by Digital Rebar during Pipeline execution to allow operators to trace jobs across a pipeline run.
* ``pipeline-uuid``": is a unique ID for each executed pipeline and set by Digital Rebar during Pipeline execution to allow operators to trace jobs across a pipeline run.

.. _rs_broker_metadata:

ResourceBroker.Meta controls
----------------------------

Used by Digital Rebar server to manage special behavior.  See :ref:`rs_data_metadata_params_old` for more detail.

* ``no-cluster: "true"`` tells the cluster and resource broker create that this parameter or profile should *NOT* be added to the special profile during object create.
* ``BaseContext: [context]`` lets DRP know what the base context is for the machine when going back to a known context
* ``machine-role: [role]`` is set by DRP and should not be edited.  It lets DRP know what the type of machine this object is.  Values can be: ``self``, ``machine``, ``cluster``, or ``resource-broker``.
* ``pipeline:``: the name of the executed pipeline and set by Digital Rebar during Pipeline execution to allow operators to trace jobs across a pipeline run.
* ``pipeline-uuid``": is a unique ID for each executed pipeline and set by Digital Rebar during Pipeline execution to allow operators to trace jobs across a pipeline run.

.. _rs_pipeline_options:

Pipeline options
----------------

The following Meta values are used to help the UX offer an simplified experience for operators.

General guidelines for required components of Pipeline metadata

The chain-map tasks and values are exceptions.

Digital Rebar adds the following Meta to help trace pipelines:

* ``pipeline:``: the name of the executed pipeline and set by Digital Rebar during Pipeline execution to allow operators to trace jobs across a pipeline run.
* ``pipeline-uuid``": is a unique ID for each executed pipeline and set by Digital Rebar during Pipeline execution to allow operators to trace jobs across a pipeline run.

.. _rs_pipeline_metadata_workflows:

Workflows.Meta
~~~~~~~~~~~~~~

* universal - describes the entry point for the workflow

  * ``start`` - entry point for pipelines
  * ``provision`` - performs provision operations for pipelines

.. _rs_pipeline_metadata_profiles:


Profiles.Meta
~~~~~~~~~~~~~

* ``pipeline`` - used to filter user created pipeline Profiles.  DO NOT include with universal-application profiles.  Use value from application. 

  * ``cluster`` - create/deletes/manages other machines
  * ``machine`` - manages the machine assigned

* ``application`` - the type of pipeline

  * ``cluster`` - create/deletes/manages other machines
  * ``machine`` - manages the machine assigned

* ``platform`` - the platform or vendor target for the pipeline

  * ``linode`` | aws | google | azure
  * ``centos`` | rocky | ubuntu | esxi | windows

* ``major`` - for versionsed systems, identifies major version
* ``minor``: - for versionsed systems, identifies minor version
* ``required`` - UX form helper ensures users are prompted to add needed values

  * for example: "linode/token,linode/root-password"

* ``optional`` - UX form helper ensures users are prompted to add common values

  * for example: "linode/region,linode/instance-type,linode/instance-image,linode/root-password"


.. _rs_pipeline_metadata_stages:

Stages.Meta
-----------

* ``workflow`` - UX helper points back to the workflow using this stage
* ``phase`` - UX helper to identify the phase of the work

  * ``base``
  * ``pre``
  * ``mid``
  * ``post``
* `action` - UX helper to identify to type of work

  * ``classification``
  * ``validation``
  * ``flexiflow``

.. _rs_metadata_tasks:

Tasks.Meta
----------

* ``flow-comment`` - Used by UX to provide side-effect information in the Triggers flow visualization
* ``triggered-by`` - Set by Digital Rebar when a Trigger uses a Blueprint to create a Work Order.  Can be retrieved in a Task using {{get .WorkOrder.Meta "triggered-by"}}

Blueprints.Meta
---------------

* ``flow-comment`` - Used by UX to provide side-effect information in the Triggers flow visualization

.. _rs_metadata_workorders:

WorkOrder.Meta
--------------

* ``triggered-by`` - Set by Digital Rebar when a Trigger uses a Blueprint to create a Work Order.  Can be retrieved in a Task using {{get .WorkOrder.Meta "triggered-by"}}

.. _rs_legacy_metadata:

Legacy & Non-Operational Meta
------------------------------

The following Meta fields are often included in objects but has no UX or operational impact on the system:

* ``feature-flags``: legacy setting needed by earlier Digital Rebar for backwards compatability.  Always set to ``sane-exit-codes``
* ``title``: provides optional authorship information.  Often set automatically to "User added" by the UX when creating new objects.
* ``copyright`` : provides optional authorship information
* ``type`` : unimplenented attempt to standardize icon and color information (please remove when found)
