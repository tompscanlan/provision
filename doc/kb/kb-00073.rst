.. Copyright (c) 2023 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license

.. REFERENCE kb-00000 for an example and information on how to use this template.
.. If you make EDITS - ensure you update footer release date information.
.. Generated from https://gitlab.com/rackn/provision/-/blob/v4/tools/docs-make-kb.sh


.. _kb_os_and_architectures_drpcli:
.. _rs_kb_00073:

kb-00073: What OS and Architectures does 'drpcli' run on?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Knowledge Base Article: kb-00073


Description
-----------

This document describes the Operating System (OSes) and system Architectures
that the ``drpcli`` CLI tool and Agent/Runner runs on.  This is also the
list of supported OSes/Architecture combos that RackN supports.


Solution
--------

The ultimate source of truth for the Published and supported versions of the
``drpcli`` binary can be found by examining the Git publish script used by
RackN to stage the published versions.  You can find it here:

  * ``https://gitlab.com/rackn/provision/-/blob/v4/tools/publish.sh``

Each of the supported binaries can also be found on your existing installed
DRP Endpoint in the ``files`` space.

As of March 2022, the current supported Arch/OS combos are as follows (please
refer to the above URL for updates after this date):

  ::

    amd64/darwin
    amd64/linux
    amd64/windows
    arm64/linux
    armv7/linux
    ppc64le/linux


Additional Information
----------------------

To find the currently compiled and supported ``drpcli`` binaries on your
DRP Endpoint, you can use one of the following methods.

  1. See the ``Files`` menu item in the Portal/UX, items beginning with ``drpcli`` in the filename, followed by the architecture and OS (eg ``drpcli.ppc64le.linux``)
  2. Using the API and the ``/files`` endpoint in the API
  3. Run the following ``drpcli`` command:

    ::

      drpcli files list | jq -r '.[] | select(startswith("drpcli"))'


See Also
========

  * :ref:`rs_supported_operating_systems`
  * :ref:`kb_building_drpcli`
  * :ref:`rs_drpcli`
  * :ref:`rs_drpcli_agent`


Versions
========

All


Keywords
========

drpcli, agent, client, cli, runner, versions, operating systems, os, oses, architectures


Revision Information
====================
  ::

    KB Article     :  kb-00073
    initial release:  Thu Mar 24 09:03:57 PDT 2022
    updated release:  Thu Mar 24 09:03:57 PDT 2022

