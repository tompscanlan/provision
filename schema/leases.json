{
  "$defs": {
    "DhcpOption": {
      "additionalProperties": false,
      "description": "DhcpOption is a representation of a specific DHCP option.",
      "properties": {
        "Code": {
          "description": "Code is a DHCP Option Code.\n\nrequired: true",
          "type": "integer"
        },
        "Value": {
          "description": "Value is a text/template that will be expanded\nand then converted into the proper format\nfor the option code\n\nrequired: true",
          "type": "string"
        }
      },
      "type": "object"
    },
    "Lease": {
      "additionalProperties": false,
      "description": "Lease tracks DHCP leases.",
      "properties": {
        "Addr": {
          "description": "Addr is the IP address that the lease handed out.\n\nrequired: true\nswagger:strfmt ipv4",
          "format": "ipv4",
          "type": "string"
        },
        "Available": {
          "description": "Available tracks whether or not the model passed validation.\nread only: true",
          "type": "boolean"
        },
        "Bundle": {
          "description": "Bundle tracks the name of the store containing this object.\nThis field is read-only, and cannot be changed via the API.\n\nread only: true",
          "type": "string"
        },
        "Duration": {
          "description": "Duration is the time in seconds for which a lease can be valid.\nExpireTime is calculated from Duration.",
          "type": "integer"
        },
        "Endpoint": {
          "description": "Endpoint tracks the owner of the object among DRP endpoints\nread only: true",
          "type": "string"
        },
        "Errors": {
          "description": "If there are any errors in the validation process, they will be\navailable here.\nread only: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "ExpireTime": {
          "description": "ExpireTime is the time at which the lease expires and is no\nlonger valid The DHCP renewal time will be half this, and the\nDHCP rebind time will be three quarters of this.\n\nrequired: true\nswagger:strfmt date-time",
          "format": "date-time",
          "type": "string"
        },
        "Meta": {
          "$ref": "#/$defs/Meta"
        },
        "NextServer": {
          "description": "NextServer is the IP address that we should have the machine talk to\nnext.  In most cases, this will be our address.\n\nrequired: false\nswagger:strfmt ipv4",
          "format": "ipv4",
          "type": "string"
        },
        "Options": {
          "description": "Options are the DHCP options that the Lease is running with.",
          "items": {
            "$ref": "#/$defs/DhcpOption"
          },
          "type": "array"
        },
        "ProvidedOptions": {
          "description": "ProvidedOptions are the DHCP options the last Discover or Offer packet\nfor this lease provided to us.",
          "items": {
            "$ref": "#/$defs/DhcpOption"
          },
          "type": "array"
        },
        "ReadOnly": {
          "description": "ReadOnly tracks if the store for this object is read-only.\nThis flag is informational, and cannot be changed via the API.\n\nread only: true",
          "type": "boolean"
        },
        "SkipBoot": {
          "description": "SkipBoot indicates that the DHCP system is allowed to offer\nboot options for whatever boot protocol the machine wants to\nuse.\n\nread only: true",
          "type": "boolean"
        },
        "State": {
          "description": "State is the current state of the lease.  This field is for informational\npurposes only.\n\nread only: true\nrequired: true",
          "type": "string"
        },
        "Strategy": {
          "description": "Strategy is the leasing strategy that will be used determine what to use from\nthe DHCP packet to handle lease management.\n\nrequired: true",
          "type": "string"
        },
        "Token": {
          "description": "Token is the unique token for this lease based on the\nStrategy this lease used.\n\nrequired: true",
          "type": "string"
        },
        "Validated": {
          "description": "Validated tracks whether or not the model has been validated.\nread only: true",
          "type": "boolean"
        },
        "Via": {
          "description": "Via is the IP address used to select which subnet the lease belongs to.\nIt is either an address present on a local interface that dr-provision is\nlistening on, or the GIADDR field of the DHCP request.\n\nrequired: false\nswagger:strfmt ipv4",
          "format": "ipv4",
          "type": "string"
        }
      },
      "type": "object"
    },
    "Meta": {
      "description": "Meta holds information about arbitrary things.",
      "patternProperties": {
        ".*": {
          "type": "string"
        }
      },
      "type": "object"
    }
  },
  "$id": "https://gitlab.com/rackn/provision/v4/models/lease",
  "$ref": "#/$defs/Lease",
  "$schema": "https://json-schema.org/draft/2020-12/schema"
}
