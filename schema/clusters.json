{
  "$defs": {
    "Cluster": {
      "additionalProperties": false,
      "description": "Cluster represents a single bare-metal system that the provisioner should manage the boot environment for.",
      "properties": {
        "Address": {
          "description": "The IPv4 address of the machine that should be used for PXE\npurposes.  Note that this field does not directly tie into DHCP\nleases or reservations -- the provisioner relies solely on this\naddress when determining what to render for a specific machine.\nAddress is updated automatically by the DHCP system if\nHardwareAddrs is filled out.\n\nswagger:strfmt ipv4",
          "format": "ipv4",
          "type": "string"
        },
        "Arch": {
          "description": "Arch is the machine architecture. It should be an arch that can\nbe fed into $GOARCH.\n\nrequired: true",
          "type": "string"
        },
        "Available": {
          "description": "Available tracks whether or not the model passed validation.\nread only: true",
          "type": "boolean"
        },
        "BootEnv": {
          "description": "The boot environment that the machine should boot into.  This\nmust be the name of a boot environment present in the backend.\nIf this field is not present or blank, the global default bootenv\nwill be used instead.",
          "type": "string"
        },
        "Bundle": {
          "description": "Bundle tracks the name of the store containing this object.\nThis field is read-only, and cannot be changed via the API.\n\nread only: true",
          "type": "string"
        },
        "Context": {
          "description": "Contexts contains the name of the current execution context.\nAn empty string indicates that an agent running on a Machine should be executing tasks,\nand any other value means that an agent running with its context set for this value should\nbe executing tasks.",
          "type": "string"
        },
        "CurrentJob": {
          "$ref": "#/$defs/UUID",
          "description": "The UUID of the job that is currently running.\n\nswagger:strfmt uuid"
        },
        "CurrentTask": {
          "description": "The index into the Tasks list for the task that is currently\nrunning (if a task is running) or the next task that will run (if\nno task is currently running).  If -1, then the first task will\nrun next, and if it is equal to the length of the Tasks list then\nall the tasks have finished running.\n\nrequired: true",
          "type": "integer"
        },
        "Description": {
          "description": "A description of this machine.  This can contain any reference\ninformation for humans you want associated with the machine.",
          "type": "string"
        },
        "Endpoint": {
          "description": "Endpoint tracks the owner of the object among DRP endpoints\nread only: true",
          "type": "string"
        },
        "Errors": {
          "description": "If there are any errors in the validation process, they will be\navailable here.\nread only: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Fingerprint": {
          "$ref": "#/$defs/MachineFingerprint",
          "description": "Fingerprint is a collection of data that can (in theory) be used to uniquely identify\na machine based on various DMI information.  This (in conjunction with HardwareAddrs)\nis used to uniquely identify a Machine using a score based on how many total items in the Fingerprint\nmatch."
        },
        "HardwareAddrs": {
          "description": "HardwareAddrs is a list of MAC addresses we expect that the system might boot from.\nThis must be filled out to enable MAC address based booting from the various bootenvs,\nand must be updated if the MAC addresses for a system change for whatever reason.",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Locked": {
          "description": "Locked indicates that changes to the Machine by users are not\nallowed, except for unlocking the machine, which will always\ngenerate an Audit event.\n\nrequired: true",
          "type": "boolean"
        },
        "Meta": {
          "$ref": "#/$defs/Meta"
        },
        "Name": {
          "description": "The name of the machine.  This must be unique across all\nmachines, and by convention it is the FQDN of the machine,\nalthough nothing enforces that.\n\nrequired: true\nswagger:strfmt hostname",
          "type": "string"
        },
        "OS": {
          "description": "OS is the operating system that the node is running in.  It is updated by Sledgehammer and by\nthe various OS install tasks.",
          "type": "string"
        },
        "Params": {
          "additionalProperties": true,
          "description": "The Parameters that have been directly set on the Machine.",
          "type": "object"
        },
        "Partial": {
          "description": "Partial tracks if the object is not complete when returned.\nread only: true",
          "type": "boolean"
        },
        "PendingWorkOrders": {
          "description": "PendingWorkOrders is the number of work orders for this\nMachine that are in the 'created' state.",
          "type": "integer"
        },
        "Pool": {
          "description": "Pool contains the pool the machine is in.\nUnset machines will join the default Pool",
          "type": "string"
        },
        "PoolAllocated": {
          "description": "PoolAllocated defines if the machine is allocated in this pool\nThis is a calculated field.",
          "type": "boolean"
        },
        "PoolStatus": {
          "description": "PoolStatus contains the status of this machine in the Pool.\n   Values are defined in Pool.PoolStatuses",
          "type": "string"
        },
        "Profiles": {
          "description": "An array of profiles to apply to this machine in order when looking\nfor a parameter during rendering.",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "ReadOnly": {
          "description": "ReadOnly tracks if the store for this object is read-only.\nThis flag is informational, and cannot be changed via the API.\n\nread only: true",
          "type": "boolean"
        },
        "RetryTaskAttempt": {
          "description": "This tracks the number of retry attempts for the current task.\nWhen a task succeeds, the retry value is reset.",
          "type": "integer"
        },
        "Runnable": {
          "description": "Runnable indicates that this is Runnable.",
          "type": "boolean"
        },
        "RunningWorkOrders": {
          "description": "RunningWorkOrders is the number of work orders for this\nMachine that are in the 'running' state.",
          "type": "integer"
        },
        "Secret": {
          "description": "Secret for machine token revocation.  Changing the secret will invalidate\nall existing tokens for this machine",
          "type": "string"
        },
        "Stage": {
          "description": "The stage that this is currently in.",
          "type": "string"
        },
        "TaskErrorStacks": {
          "description": "This list of previous task lists and current tasks to handle errors.\nUpon completing the list, the previous task list will be executed.\n\nThis will be capped to a depth of 1.  Error failures can not be handled.",
          "items": {
            "$ref": "#/$defs/TaskStack"
          },
          "type": "array"
        },
        "Tasks": {
          "description": "The current tasks that are being processed.",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Uuid": {
          "$ref": "#/$defs/UUID",
          "description": "The UUID of the machine.\nThis is auto-created at Create time, and cannot change afterwards.\n\nrequired: true\nswagger:strfmt uuid"
        },
        "Validated": {
          "description": "Validated tracks whether or not the model has been validated.\nread only: true",
          "type": "boolean"
        },
        "WorkOrderMode": {
          "description": "WorkOrderMode indicates if the machine is action mode",
          "type": "boolean"
        },
        "Workflow": {
          "description": "Workflow is the workflow that is currently responsible for processing machine tasks.\n\nrequired: true",
          "type": "string"
        },
        "WorkflowComplete": {
          "description": "WorkflowComplete indicates if the workflow is complete",
          "type": "boolean"
        }
      },
      "type": "object"
    },
    "MachineFingerprint": {
      "additionalProperties": false,
      "description": "MachineFingerprint defines a set of values that identify a machine.",
      "properties": {
        "CSNHash": {
          "contentEncoding": "base64",
          "description": "DMI.System.Manufacturer + DMI.System.ProductName + DMI.Chassis[0].SerialNumber, SHA256 hashed\nHash must not be zero-length to match. 25 points",
          "type": "string"
        },
        "CloudInstanceID": {
          "description": "Cloud-init file in /run/cloud-init/instance-data.json\nString from ID of '.v1.cloud_name' + '.v1.instance_id'. 500 point match",
          "type": "string"
        },
        "MemoryIds": {
          "description": "MemoryIds is an array of SHA256sums if the following fields in each\nentry of the DMI.Memory.Devices array concatenated together:\n * Manufacturer\n * PartNumber\n * SerialNumber\nEach hash must not be zero length\nScore is % matched.",
          "items": {
            "contentEncoding": "base64",
            "type": "string"
          },
          "type": "array"
        },
        "SSNHash": {
          "contentEncoding": "base64",
          "description": "DMI.System.Manufacturer + DMI.System.ProductName + DMI.System.SerialNumber, SHA256 hashed\nHash must not be zero-length to match. 25 points",
          "type": "string"
        },
        "SystemUUID": {
          "description": "DMI.System.UUID, not hashed. Must be non zero length and must be a non-zero UUID. 50 point match",
          "type": "string"
        }
      },
      "type": "object"
    },
    "Meta": {
      "description": "Meta holds information about arbitrary things.",
      "patternProperties": {
        ".*": {
          "type": "string"
        }
      },
      "type": "object"
    },
    "TaskStack": {
      "additionalProperties": false,
      "description": "TaskStack contains a stack of tasks.",
      "properties": {
        "CurrentTask": {
          "type": "integer"
        },
        "TaskList": {
          "items": {
            "type": "string"
          },
          "type": "array"
        }
      },
      "type": "object"
    },
    "UUID": {
      "contentEncoding": "base64",
      "type": "string"
    }
  },
  "$id": "https://gitlab.com/rackn/provision/v4/models/cluster",
  "$ref": "#/$defs/Cluster",
  "$schema": "https://json-schema.org/draft/2020-12/schema"
}