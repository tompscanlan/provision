package cli

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/tftp/v3"
	"io"
	"net"
	"net/http"
	"os"
)

func staticCommands() *cobra.Command {
	useHttp := false
	useHttps := false
	useTftp := false
	cmd := &cobra.Command{
		Use:   "static",
		Short: "Retrieve and uplaod files using the static HTTP, HTTPS, and TFTP servers",
	}
	cmd.PersistentFlags().BoolVar(&useHttp, "http", false, "Use HTTP port for transfers")
	cmd.PersistentFlags().BoolVar(&useHttps, "https", false, "Use HTTPS port for transfers")
	cmd.PersistentFlags().BoolVar(&useTftp, "tftp", false, "Use TFTP for transfers")
	otherSide := func(info *models.Info, remotePath string) (string, error) {
		if !(useHttp || useHttps || useTftp) {
			useHttp = info.SecureFilePort == 0 || !info.SecureProvisionerEnabled
			useHttps = !useHttp
		}
		switch {
		case useHttp:
			if info.FilePort == 0 || !info.ProvisionerEnabled {
				return "", fmt.Errorf("Static file server not enabled")
			}
			host := net.JoinHostPort(Session.Host(), fmt.Sprintf("%d", info.FilePort))
			return fmt.Sprintf("http://%s/%s", host, remotePath), nil
		case useHttps:
			if info.SecureFilePort == 0 || !info.SecureProvisionerEnabled {
				return "", fmt.Errorf("Secure static file server not enabled")
			}
			host := net.JoinHostPort(Session.Host(), fmt.Sprintf("%d", info.SecureFilePort))
			return fmt.Sprintf("https://%s/%s", host, remotePath), nil
		case useTftp:
			if info.TftpPort == 0 || !info.TftpEnabled {
				return "", fmt.Errorf("TFTP server not enabled")
			}
			return net.JoinHostPort(Session.Host(), fmt.Sprintf("%d", info.TftpPort)), nil
		default:
			return "", fmt.Errorf("Cannot happen")
		}
	}
	cmd.AddCommand(&cobra.Command{
		Use:   "upload [file] to [dest]",
		Short: "Upload the local [file] to [dest] on the static file server",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 3 {
				return fmt.Errorf("Need 2 args")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			info, err := Session.Info()
			if err != nil {
				return err
			}
			src, err := os.Open(args[0])
			if src != nil {
				defer src.Close()
			}
			if err != nil {
				return err
			}
			dest, err := otherSide(info, args[2])
			if err != nil {
				return err
			}
			switch {
			case useHttp, useHttps:
				client := Session.StaticClient()
				resp, err := client.Post(dest, "application/octet-stream", src)
				if resp != nil && resp.Body != nil {
					defer resp.Body.Close()
				}
				if err != nil {
					return err
				}
				if resp.Body != nil {
					io.Copy(os.Stderr, resp.Body)
				}
				if resp.StatusCode >= 400 {
					return fmt.Errorf("Upload to %s failed: status %s", dest, resp.Status)
				}
			case useTftp:
				client, err := tftp.NewClient(dest)
				if err != nil {
					return err
				}
				client.SetBlockSize(16000)
				payload, err := client.Send(args[2], "octet")
				if err != nil {
					return err
				}
				if _, err := payload.ReadFrom(src); err != nil {
					return fmt.Errorf("Upload of %s failed: %v", args[0], err)
				}
			}
			fmt.Printf("Uploaded %s to %s\n", args[0], args[2])
			return nil
		},
	})
	cmd.AddCommand(&cobra.Command{
		Use:   "download [src] to [file]",
		Short: "Download [src] from the static file server and save it to [file]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 3 {
				return fmt.Errorf("Need 2 args")
			}
			return nil
		},

		RunE: func(c *cobra.Command, args []string) error {
			info, err := Session.Info()
			if err != nil {
				return err
			}
			dest, err := os.Create(args[2] + ".tmp")
			if dest != nil {
				defer dest.Close()
				defer os.Remove(args[2] + ".tmp")
			}
			if err != nil {
				return err
			}
			src, err := otherSide(info, args[0])
			if err != nil {
				return err
			}
			switch {
			case useHttp, useHttps:
				client := Session.StaticClient()
				resp, err := client.Get(src)
				if resp != nil && resp.Body != nil {
					defer resp.Body.Close()
				}
				if err != nil {
					return err
				}
				if resp.StatusCode >= 300 {
					io.Copy(os.Stderr, resp.Body)
					return fmt.Errorf("Download of %s failed: status %s", src, resp.Status)
				}
				if _, err := io.Copy(dest, resp.Body); err != nil {
					return fmt.Errorf("Error saving to %s: %v", args[2], err)
				}
			case useTftp:
				client, err := tftp.NewClient(src)
				if err != nil {
					return err
				}
				client.SetBlockSize(16000)
				payload, err := client.Receive(args[0], "octet")
				if err != nil {
					return err
				}
				if _, err := payload.WriteTo(dest); err != nil {
					return fmt.Errorf("Download of %s failed: %v", args[2], err)
				}
			}
			if err := dest.Close(); err != nil {
				return err
			}
			os.Rename(args[2]+".tmp", args[2])
			fmt.Printf("Downloaded %s to %s\n", args[0], args[2])
			return nil
		},
	})
	cmd.AddCommand(&cobra.Command{
		Use:   "check [src] equals [dest]",
		Short: "Check to see if [dest] is already identical to [src] based on calculated sha256sum",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 3 {
				return fmt.Errorf("Need 2 args")
			}
			if useTftp {
				return fmt.Errorf("TFTP does not have Etag support, cannot check for remote equality")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			fi, err := os.Open(args[2])
			if err != nil {
				return err
			}
			defer fi.Close()
			mts := &models.ModTimeSha{}
			if mts.Regenerate(fi); len(mts.ShaSum) == 0 {
				return fmt.Errorf("Failed to calc local sha256sum")
			}
			fi.Close()
			info, err := Session.Info()
			if err != nil {
				return err
			}
			src, err := otherSide(info, args[0])
			if err != nil {
				return err
			}
			req, err := http.NewRequest("HEAD", src, nil)
			if err != nil {
				return err
			}
			shaHdr := `"SHA256:` + mts.String() + `"`
			req.Header.Set("If-None-Match", shaHdr)
			client := Session.StaticClient()
			resp, err := client.Do(req)
			if err != nil {
				return err
			}
			if resp.Body != nil {
				resp.Body.Close()
			}
			if resp.StatusCode == http.StatusNotModified {
				fmt.Printf("%s is identical to %s\n", args[0], args[2])
				return nil
			}
			if resp.StatusCode >= 299 {
				return fmt.Errorf("error checking %s: %v", args[2], resp.Status)
			}
			if hdr := resp.Header.Get("ETag"); hdr == shaHdr {
				fmt.Printf("%s is identical to %s\n", args[0], args[2])
				return nil
			}
			return fmt.Errorf("%s has changed", args[2])
		},
	})
	return cmd
}

func init() {
	addRegistrar(registerStatic)
}

func registerStatic(app *cobra.Command) {
	app.AddCommand(staticCommands())
}
