//go:build !unix

package cli

import (
	"os/exec"
)

func reapChildren(cmd *exec.Cmd) error {
	return cmd.Run()
}
