package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerFilter)
}

func registerFilter(app *cobra.Command) {
	op := &ops{
		name:       "filters",
		singleName: "filter",
		example:    func() models.Model { return &models.Filter{} },
	}
	op.command(app)
}
