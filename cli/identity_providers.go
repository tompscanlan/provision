package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerIdentityProvider)
}

func registerIdentityProvider(app *cobra.Command) {
	op := &ops{
		name:       "identity_providers",
		singleName: "identity_provider",
		example:    func() models.Model { return &models.IdentityProvider{} },
	}
	op.command(app)
}
