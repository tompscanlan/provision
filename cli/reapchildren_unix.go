//go:build unix

package cli

import (
	"os"
	exec "os/exec"
	"syscall"
)

func reapChildren(cmd *exec.Cmd) error {
	if err := cmd.Start(); err != nil {
		return err
	}
	var ws syscall.WaitStatus
	for {
		pid, _ := syscall.Wait4(-1, &ws, 0, nil)
		if pid == cmd.Process.Pid && ws.Exited() {
			os.Exit(ws.ExitStatus())
		}
	}
}
