#!/usr/bin/env bash

echo "{"
for i in `ls schema`
do

        fp=$(pwd)/schema/$i
        n=$(basename $i .json)
        echo "${COMMA}\"$fp\": \"$n/*.{json,yaml,yml}\""
        COMMA=","
done
echo "}"

