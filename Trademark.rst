
.. _rs_Trademark:

Trademark
=========

The Digital Rebar name and mark are maintained by RackN, RackN requests vendors obtain permission for commercial uses.

Name
----

Digital Rebar usage options are as follows:

* The project name is "Digital Rebar" as two words, both capitalized
* Acceptable alternatives:

  * DigitalRebar (avoid in written text in favor of Digital Rebar)
  * rebar.digital (the "." is required in this format)
  * dR or DR  (do not use Dr)
  * Internally within the project, it is acceptable to just say "Rebar"

Logos
-----

It is acceptable to use the Digital Rebar logos when referencing the project or workloads that leverage the project.

Large Icon:

.. image:: /doc/images/digital_rebar_small.png

Small Icon

.. image:: /doc/images/digitalrebar.ico
   :height: 25px

Mascot
------

Cloud Native Metal Bear
